package fi.virta.di

import fi.virta.core.prefs.IUserPreferences
import fi.virta.core.prefs.UserPreferences
import fi.virta.data.LoginDataSource
import fi.virta.data.LoginRepository
import fi.virta.data.StationRepository
import fi.virta.ui.login.LoginViewModel
import fi.virta.ui.main.MainViewModel
import fi.virta.ui.stations.StationListViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.core.scope.ScopeID
import org.koin.dsl.module

/**
 * Login Activity Module
 */
val loginModule = module {
    single { LoginDataSource(get()) }
    single { LoginRepository(get()) }
    single { UserPreferences(androidApplication()) as IUserPreferences }

    scope(named("login")){
        scoped { LoginDataSource(get()) }
        scoped { LoginRepository(get()) }
    }

//    viewModel { (scopeId : ScopeID) -> LoginViewModel(get(), getScope(scopeId).get())}
    viewModel { LoginViewModel(get(), get()) }
}


/**
 * Main Activity Module
 */
val mainModule = module {

//    single { UserPreferences(androidApplication()) as IUserPreferences }
//    viewModel { (scopeId : ScopeID) -> MainViewModel(getScope(scopeId).get()) }
    viewModel { MainViewModel(get())}
}

/**
 * Station List Fragment Module
 */
val stationListModule = module {

    single { StationRepository(get()) }
    viewModel { StationListViewModel(get(), get()) }
}

val appModules = listOf(networkModule, loginModule, mainModule, stationListModule)