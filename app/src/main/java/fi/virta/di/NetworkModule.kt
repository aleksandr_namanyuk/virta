package fi.virta.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fi.virta.BuildConfig
import fi.virta.core.VirtaApi
import fi.virta.core.network.DateTimeConverter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.DateTime
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideVirtaApiService(get()) }
    single { provideGson() }
    single { provideRetrofit(get(), get()) }
    single { provideOkHttpClient() }
}



fun provideOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()
    builder.connectTimeout(30L, TimeUnit.SECONDS)
    builder.readTimeout(30L, TimeUnit.SECONDS)
    builder.writeTimeout(30L, TimeUnit.SECONDS)

    if(BuildConfig.ENABLE_NETWORK_LOGGING){
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        builder.addInterceptor(httpLoggingInterceptor)
    }

    return builder.build()
}

fun provideGson(): Gson =
    GsonBuilder()
        .registerTypeAdapter(DateTime::class.java, DateTimeConverter())
        .create()

fun provideRetrofit(okHttpClient: OkHttpClient,
                    gson: Gson) : Retrofit =
    Retrofit.Builder()
        .baseUrl(BuildConfig.API_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
//        .addCallAdapterFactory(LiveDataCallAdapterFactory())
        .client(okHttpClient)
        .build()

fun provideVirtaApiService(retrofit: Retrofit) : VirtaApi =
    retrofit.create(VirtaApi::class.java)