package fi.virta.models

import com.google.gson.annotations.SerializedName

data class AuthResponse(val token: String?,
                        @SerializedName("status_code") val statusCode: Int?,
                        val message: String?,
                        @SerializedName("error_code") val errorCode: Int?)