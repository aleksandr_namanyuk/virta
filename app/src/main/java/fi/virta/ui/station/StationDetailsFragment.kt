package fi.virta.ui.station

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import fi.virta.R
import fi.virta.core.autoCleared
import fi.virta.data.model.Station
import fi.virta.databinding.FragmentDetailsStationBinding
import kotlinx.android.synthetic.main.activity_login.view.toolbar
import kotlinx.android.synthetic.main.fragment_details_station.view.*
import timber.log.Timber


class StationDetailsFragment: Fragment(){

    companion object {

        const val KEY_STATION = "station"

        fun newInstance(station: Station): StationDetailsFragment {

            val bundle = Bundle()
            bundle.putParcelable(KEY_STATION, station)

            return StationDetailsFragment().apply {
                arguments = bundle
            }
        }
    }

    var binding by autoCleared<FragmentDetailsStationBinding>()

    lateinit var station: Station

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        Timber.v("Create Station Fragment ")

        val dataBinding = DataBindingUtil.inflate<FragmentDetailsStationBinding>(
            inflater, R.layout.fragment_details_station, container, false)

        with(activity as AppCompatActivity){
            setSupportActionBar(dataBinding.root.toolbar)
            supportActionBar?.title = ""
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)

            dataBinding.root.toolbar.setNavigationOnClickListener {
                supportFragmentManager.popBackStack()

            }
        }

        (arguments?.getParcelable(KEY_STATION) as Station).let {
            station = it
        }

        binding = dataBinding


        // TODO : use custom layout for each connector
        station.evses?.forEach {
            it.connectors?.forEach { connector ->
                Timber.v("ev maxkw : ${connector.maxkw}" )
                val textview = TextView(activity)
                textview.text = connector.maxkw.toString() + " kW"

                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(50, 0, 16, 30)

                textview.layoutParams = params

                dataBinding.root.connectors.addView(textview)
            }
        }

        return dataBinding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {

        binding.lifecycleOwner = viewLifecycleOwner
        binding.station = station

    }

}