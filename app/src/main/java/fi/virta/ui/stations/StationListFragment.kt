package fi.virta.ui.stations

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.LatLng
import fi.virta.R
import fi.virta.core.autoCleared
import fi.virta.databinding.FragmentListStationBinding
import fi.virta.presentation.StationAdapter
import fi.virta.ui.station.StationDetailsFragment
import kotlinx.android.synthetic.main.fragment_list_station.*
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class StationListFragment: Fragment(){

    private val stationListViewModel: StationListViewModel by viewModel()

    var binding by autoCleared<FragmentListStationBinding>()

    var adapter by autoCleared<StationAdapter>()

    private var locationManager : LocationManager? = null

    companion object {

        fun newInstance(): StationListFragment {
            return StationListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        Timber.v("Create Station Fragment ")

        val dataBinding = DataBindingUtil.inflate<FragmentListStationBinding>(
            inflater, R.layout.fragment_list_station, container, false)
        binding = dataBinding

        return dataBinding.root
    }

    override fun onViewCreated(view: View,
                               savedInstanceState: Bundle?) {

        binding.lifecycleOwner = viewLifecycleOwner
        binding.stations = stationListViewModel.stationList

        iniLocationManager()
        stationListViewModel.getStations()

        initAdapter()
        subscribeToStationList()
    }

    fun initAdapter(){

        val layoutManager = LinearLayoutManager(context)
        stationList.layoutManager = layoutManager

        val adapter = StationAdapter { station ->
            Timber.v("Station clicked id : ${station.id}")

            val detailsFragment =
                StationDetailsFragment.newInstance(station)

            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.replace(R.id.frameLayout, detailsFragment, "stationDetails")
                ?.addToBackStack(null)
                ?.commit()

        }

        this.adapter = adapter
        binding.stationList.adapter = adapter

    }

    private fun subscribeToStationList(){

        stationListViewModel.stationList.observe (viewLifecycleOwner, Observer {
            Timber.v("Stations ... size . ${it.size}")

            val stations = it ?: return@Observer showNothing()

            if(stations.isNotEmpty()){
                val stationUiList = adapter.buildStationCollectionFromData(it)
                adapter.submitList(stationUiList)
            } else {
                showNothing()
            }

        })
    }

    private fun showNothing(){
        adapter.submitList(emptyList())
    }

    private fun iniLocationManager(){
        locationManager = activity?.getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager?

        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f,
                object : LocationListener {

                    override fun onLocationChanged(location: Location) {
                        Timber.v("latitude: ${location.latitude} & longitude: ${location.longitude} ")

                        val leftTopPoint = getDestinationPoint(
                            LatLng(location.latitude, location.longitude),
                            315.0,
                            10.0)

                        val rightBottomPoint = getDestinationPoint(
                            LatLng(location.latitude, location.longitude),
                            135.0,
                            10.0
                        )

                        if(leftTopPoint == null || rightBottomPoint == null)
                            return

                        Timber.v("lat: ${leftTopPoint.latitude} lon: ${leftTopPoint.longitude}")
                        Timber.v("lat: ${rightBottomPoint.latitude} lon: ${rightBottomPoint.longitude}")

                        val latMin = if(leftTopPoint.latitude < rightBottomPoint.latitude)
                            leftTopPoint.latitude
                        else
                            rightBottomPoint.latitude

                        val latMax = if(leftTopPoint.latitude < rightBottomPoint.latitude)
                            rightBottomPoint.latitude
                        else
                            leftTopPoint.latitude

                        val lonMin = if(leftTopPoint.longitude < rightBottomPoint.longitude)
                            leftTopPoint.longitude
                        else
                            rightBottomPoint.longitude

                        val lonMax = if(leftTopPoint.longitude < rightBottomPoint.longitude)
                            rightBottomPoint.longitude
                        else
                            leftTopPoint.longitude

                        Timber.v("latMin : $latMin")

                        if(stationListViewModel.isUserLoggedIn()) {
                            stationListViewModel.getStations(latMin, latMax, lonMin, lonMax)
                        }
                    }

                    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                    override fun onProviderEnabled(provider: String) {}
                    override fun onProviderDisabled(provider: String) {}
                }
            )
        } catch(ex: SecurityException) {
            Timber.d("Security problem, no location available")
        }
    }

    private fun getDestinationPoint(source: LatLng, bearing: Double, distance: Double):LatLng? {

        var brng = bearing
        var dist = distance
        dist /= 6371
        brng = Math.toRadians(brng)

        val lat1 = Math.toRadians(source.latitude)
        val lon1 = Math.toRadians(source.longitude)
        val lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng))
        val lon2 = (lon1 + Math.atan2(
            Math.sin(brng) * Math.sin(dist) *
                    Math.cos(lat1),
            (Math.cos(dist) - (Math.sin(lat1) * Math.sin(lat2)))))

        return if (java.lang.Double.isNaN(lat2) || java.lang.Double.isNaN(lon2)) {
            null
        } else LatLng(Math.toDegrees(lat2), Math.toDegrees(lon2))
    }


}