package fi.virta.ui.stations

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fi.virta.BuildConfig
import fi.virta.BuildConfig.STATION_LIMIT
import fi.virta.core.prefs.IUserPreferences
import fi.virta.data.StationRepository
import fi.virta.data.model.Station
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber

class StationListViewModel(private val stationRepository: StationRepository,
                           private val userPreferences: IUserPreferences
) : ViewModel(){


    private val _stationList = MutableLiveData<List<Station>>()
    val stationList: LiveData<List<Station>> = _stationList

    private val errorHandler = CoroutineExceptionHandler { _, error ->
//        TODO : implement error handling
        Timber.e(error)
//        _mainResult.value = Result.Error(error.localizedMessage)
    }

    fun getStations(latMin: Double? = null,
                    latMax: Double? = null,
                    lonMin: Double? = null,
                    lonMax: Double? = null
    ) {

        Timber.v("Get Stations ")

        viewModelScope.launch(errorHandler) {
            val stations = stationRepository.loadStations(latMin, latMax, lonMin, lonMax, STATION_LIMIT)
            _stationList.value = stations
        }
    }

    fun isUserLoggedIn(): Boolean {
        return userPreferences.getToken()?.isNotEmpty() ?: false
    }

}