package fi.virta.ui.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.LatLng
import fi.virta.R
import fi.virta.ui.login.LoginActivity
import fi.virta.ui.stations.StationListFragment
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber


class MainActivity: AppCompatActivity(){

    private val mainViewModel: MainViewModel by viewModel()

    private var locationManager : LocationManager? = null

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 99
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestLocation()
        mainViewModel.isLogInRequired()
        subscribeToUserState()
//        iniLocationManager()

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.frameLayout, StationListFragment.newInstance(), "StationsListFragment")
                .commit()
        }

    }

    private fun subscribeToUserState(){

        mainViewModel.userState.observe(this@MainActivity, Observer { token ->
            // TODO : decrypt token
            if (token.isNullOrEmpty()) {
                val myIntent = Intent(this, LoginActivity::class.java)
                this.startActivity(myIntent)
            }

        })
    }

    private fun requestLocation(){
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Timber.v("Location permission granted")
            // TODO : subscribe to location change
        } else {
            Timber.w("Location permission denied")
        }
    }

    //    private fun iniLocationManager(){
//        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
//
//        try {
//            // Request location updates
//            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f,
//                object : LocationListener {
//
//                    override fun onLocationChanged(location: Location) {
//                        Timber.v("latitude: ${location.latitude} & longitude: ${location.longitude} ")
//
//                        val leftTopPoint = getDestinationPoint(
//                            LatLng(location.latitude, location.longitude),
//                            315.0,
//                            10.0)
//
//                        val rightBottomPoint = getDestinationPoint(
//                            LatLng(location.latitude, location.longitude),
//                            135.0,
//                            10.0
//                        )
//
//                        Timber.v("lat: ${leftTopPoint?.latitude} lon: ${leftTopPoint?.longitude}")
//                        Timber.v("lat: ${rightBottomPoint?.latitude} lon: ${rightBottomPoint?.longitude}")
//
//
//                    }
//
//                    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
//                    override fun onProviderEnabled(provider: String) {}
//                    override fun onProviderDisabled(provider: String) {}
//                }
//            )
//        } catch(ex: SecurityException) {
//            Timber.d("Security problem, no location available")
//        }
//    }

//    private fun getDestinationPoint(source: LatLng, bearing: Double, distance: Double):LatLng? {
//
//        var brng = bearing
//        var dist = distance
//        dist /= 6371
//        brng = Math.toRadians(brng)
//
//        val lat1 = Math.toRadians(source.latitude)
//        val lon1 = Math.toRadians(source.longitude)
//        val lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng))
//        val lon2 = (lon1 + Math.atan2(
//            Math.sin(brng) * Math.sin(dist) *
//            Math.cos(lat1),
//        (Math.cos(dist) - (Math.sin(lat1) * Math.sin(lat2)))))
//
//        return if (java.lang.Double.isNaN(lat2) || java.lang.Double.isNaN(lon2)) {
//            null
//        } else LatLng(Math.toDegrees(lat2), Math.toDegrees(lon2))
//    }

}