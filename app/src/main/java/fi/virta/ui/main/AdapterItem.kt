package fi.virta.ui.main

import fi.virta.data.model.Station

/**
 * Common class for items that are used in adapter all together.
 */
abstract class AdapterItem(val itemType: Int){

    companion object {

        const val ITEM_TYPE_STATION = 0

    }

    fun isTheSameItem(other: AdapterItem): Boolean =
        itemType == other.itemType && isSame(other)

    fun isTheContentSame(other: AdapterItem): Boolean =
        itemType == other.itemType && isContentSame(other)

    /**
     * Support for DiffUtil
     *
     * @param other [AdapterItem] to compare.
     * @return true if this [AdapterItem] presents same publicationDetails as the other.
     */
    protected abstract fun isSame(other: AdapterItem): Boolean

    /**
     * Support for DiffUtils
     *
     * @param other [AdapterItem] to compare.
     * @return true if this [AdapterItem] has the same content as the other.
     */
    protected abstract fun isContentSame(other: AdapterItem): Boolean
}

class ItemStation(
    val station: Station
) : AdapterItem(ITEM_TYPE_STATION) {

    override fun isSame(other: AdapterItem): Boolean {
        return itemType == other.itemType
    }

    override fun isContentSame(other: AdapterItem): Boolean {
        val otherItem = (other as ItemStation).station
        return station.id == station.id
    }
}
