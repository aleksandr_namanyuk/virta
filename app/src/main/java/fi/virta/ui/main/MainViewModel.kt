package fi.virta.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fi.virta.core.prefs.IUserPreferences
import fi.virta.data.StationRepository
import fi.virta.data.model.Station
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel(private val userPreferences: IUserPreferences) : ViewModel() {

    private val _userState = MutableLiveData<String?>()
    val userState: LiveData<String?> = _userState


    private val errorHandler = CoroutineExceptionHandler { _, error ->
        //        TODO : implement error handling
        Timber.e(error)
//        _mainResult.value = Result.Error(error.localizedMessage)
    }

    fun isLogInRequired() {
        Timber.v("is user logged in : ${userPreferences.getToken()}")
        _userState.value = userPreferences.getToken()
    }



}