package fi.virta.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import fi.virta.data.LoginRepository
import fi.virta.data.Result

import fi.virta.R
import fi.virta.core.prefs.IUserPreferences
import fi.virta.core.prefs.UserPreferences
import fi.virta.core.token.CredentialManager
import fi.virta.data.model.LoggedInUser
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception

class LoginViewModel(private val loginRepository: LoginRepository,
                     private val userPreferences: IUserPreferences) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    val errorHandler = CoroutineExceptionHandler { _, error ->
        _loginResult.value = Result.Error(error.localizedMessage)
    }

    private val _loginResult = MutableLiveData<Result<LoggedInUser>>()
    val loginResult: LiveData<Result<LoggedInUser>> = _loginResult

    fun login(username: String, password: String) {

        Timber.v("Saved token : ${userPreferences.getToken()}")

        viewModelScope.launch(errorHandler) {

                val response = loginRepository.login(username, password)

                response.token?.let { accessToken ->

                    Timber.v("Token : $accessToken")

                    userPreferences.setToken(accessToken)

                    // TODO : 2.1 login.token save token

//                    val cm = CredentialManager()
//                    val encryptedPrivateToken = cm.encrypt(it)
//                    Timber.v("Enc token : $encryptedPrivateToken")

                    val user = LoggedInUser(response.token)
                    _loginResult.value = Result.Success(user)
                }

        }
    }

    fun loginDataChanged(username: String, password: String) {

        if(!isUserNameValid(username))
            return // show username error

        if(!isPasswordValid(password))
            return  // show password error

        _loginForm.value = LoginFormState(isDataValid = true)
    }

    /** A placeholder username validation check */
    private fun isUserNameValid(username: String)=
        username.isNotBlank()

    /**  A placeholder password validation check */
    private fun isPasswordValid(password: String) =
        password.length > 2

}
