package fi.virta.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Station (

    @SerializedName("id")  val id: Int = 0,

    @SerializedName("latitude")  val latitude: Double = 0.toDouble(),

    @SerializedName("longitude")  val longitude: Double = 0.toDouble(),

    @SerializedName("name")  val name: String? = null,

    @SerializedName("city")  val city: String? = null,

    @SerializedName("provider")  val provider: String? = null,

    @SerializedName("evses") val evses: @RawValue List<Evses>? = null

) : Parcelable

data class Evses (

    @SerializedName("id")  val id: Int = 0,

    @SerializedName("connectors")  val connectors: List<Connector>? = null,

    @SerializedName("groupName")  val groupname: String? = null
)

data class Connector (

    @SerializedName("type")  val type: String? = null,

    @SerializedName("maxKw")  val maxkw: Float = 0.toFloat()
)