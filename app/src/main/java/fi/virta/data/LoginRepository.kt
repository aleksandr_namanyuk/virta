package fi.virta.data

import androidx.lifecycle.LiveData
import fi.virta.core.VirtaApi
import fi.virta.data.model.LoggedInUser
import fi.virta.models.AuthRequest
import fi.virta.models.AuthResponse

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(private val api: VirtaApi) {

    // in-memory cache of the loggedInUser object
    var user: LoggedInUser? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun logout() {
        user = null
//        dataSource.logout()
    }

    suspend fun login(username: String, password: String): AuthResponse {
        return api.authenticate(AuthRequest(username, password))
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }
}
