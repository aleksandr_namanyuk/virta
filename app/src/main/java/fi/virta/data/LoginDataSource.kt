package fi.virta.data

import fi.virta.core.VirtaApi
import fi.virta.data.model.LoggedInUser
import fi.virta.models.AuthRequest

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val api: VirtaApi) {

    suspend fun login(username: String, password: String): Result<LoggedInUser> {

        val response = api.authenticate(AuthRequest(username, password))

        return if(response.token != null){
            val user = LoggedInUser(response.token)
            Result.Success(user)
        } else {
            Result.Error(response.message!!)
        }


//        response.token?.let {
//            val user = LoggedInUser(response.token)
//            return Result.Success(user)
//        }

//        response.statusCode?.let {
//            response.message?.let { m ->
//                return Result.Error(m)
//            }
//        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}

