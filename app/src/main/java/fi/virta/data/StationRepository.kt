package fi.virta.data

import fi.virta.core.VirtaApi
import fi.virta.data.model.Station

class StationRepository (private val api: VirtaApi){

    suspend fun loadStations(
        latMin: Double?,
        latMax: Double?,
        lonMin: Double?,
        lonMax: Double?,
        limit: Int?
    ): List<Station> =
        api.getStations(limit, latMin, latMax, lonMin, lonMax)


}