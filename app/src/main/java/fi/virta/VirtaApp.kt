package fi.virta

import android.app.Application
import fi.virta.core.log.VirtaLog
import fi.virta.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class VirtaApp: Application(){

    override fun onCreate() {
        super.onCreate()

        VirtaLog.init()

        Timber.v("onCreate()")

        startKoin {
            androidLogger()
            androidContext(this@VirtaApp)
            modules(appModules)
        }
    }
}