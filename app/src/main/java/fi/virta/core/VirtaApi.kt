package fi.virta.core

import fi.virta.data.model.Station
import fi.virta.models.AuthRequest
import fi.virta.models.AuthResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface VirtaApi{

    @POST("auth/")
    suspend fun authenticate(@Body authRequest: AuthRequest): AuthResponse

    @GET("stations/")
    suspend fun getStations(
        @Query("limit") limit: Int?,
        @Query("latMin") latMin: Double? = null,
        @Query("latMax") latMax: Double? = null,
        @Query("longMin") longMin: Double? = null,
        @Query("longMax") longMax: Double? = null
    ): List<Station>

}