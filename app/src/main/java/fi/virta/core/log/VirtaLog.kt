package fi.virta.core.log

import fi.virta.BuildConfig
import timber.log.Timber

class VirtaLog {

    companion object {

        fun init(){
            if(BuildConfig.DEBUG){
                Timber.plant(DebugTree())
            } else {
                Timber.plant(ReleaseTree())
            }
        }
    }

}