package fi.virta.core.log

import android.util.Log
import timber.log.Timber

class DebugTree: Timber.DebugTree(){

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when(priority){
            Log.ASSERT -> {}
        }

        super.log(priority, tag, message, t)
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        return "${super.createStackElementTag(element)}:${element.lineNumber}"
    }
}