package fi.virta.core.prefs

import android.content.Context
import android.content.SharedPreferences

interface IUserPreferences{

    fun setToken(token: String?)

    fun getToken(): String?

}

class UserPreferences(private val context: Context): IUserPreferences {

    companion object {
        const val USER_DATA_STORAGE = "UserDataStorage"
        const val KEY_TOKEN = "key_token"
    }


    private val preferences: SharedPreferences by lazy {
        context.getSharedPreferences(USER_DATA_STORAGE, Context.MODE_PRIVATE)
    }

    override fun setToken(token: String?) {
        preferences.edit().putString(KEY_TOKEN, token).apply()
    }

    override fun getToken(): String? {
        return preferences.getString(KEY_TOKEN, "")
    }
}

