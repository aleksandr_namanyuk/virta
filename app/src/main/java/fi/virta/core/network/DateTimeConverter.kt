package fi.virta.core.network

import com.google.gson.*
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import java.lang.reflect.Type

class DateTimeConverter : JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    override fun serialize(src: DateTime?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement? {
        return if (src != null)
            JsonPrimitive(ISODateTimeFormat.dateTime().print(src))
        else
            null
    }

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): DateTime? {
        val date = json?.asString
        return try {
            ISODateTimeFormat.dateTime().parseDateTime(date)
        } catch (exp: Exception) {
            null
        }
    }
}