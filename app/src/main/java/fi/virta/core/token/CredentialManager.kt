package fi.virta.core.token

import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class CredentialManager {

    // TODO : 2.2 login.token.safety implement correct token save

//    fun encryptToken(){
//
//        val ENCRYPTION_ALIAS = "agdgfiu3g27^F*@$19fb9uwebnnw0mc0mlewjhw0hr03hofj19853y189rhfcnir914-g47956y3981gufbc9nr"
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
//            val generator = KeyGenerator.getInstance(KEY_ALGORITHM_RSA, "AndroidKeyStore")
//
//            val keyGenParameterSpec = KeyGenParameterSpec.Builder(ENCRYPTION_ALIAS,
//                KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
//                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
//                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
//                .build()
//
//            generator.init(keyGenParameterSpec)
//            val secretKey = generator.generateKey()
//            val cipher = Cipher.getInstance("AES/GCM/NoPadding")
//            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
//
//            val iv = cipher.iv
//            val encryption = cipher.doFinal(textToEncrypt.getBytes("UTF-8"));
//
//        } else {
//
//            val start = Calendar.getInstance()
//            val end = Calendar.getInstance()
//            end.add(Calendar.YEAR, 1);
//            KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec
//                .Builder(SadadApplication.getInstance().getApplicationContext())
//                .setAlias(ENCRYPTION_ALIAS)
//                .setSubject(new X500Principal("CN=Your Company ," +
//                        " O=Your Organization" +
//                        " C=Your Coountry"))
//                .setSerialNumber(BigInteger.ONE)
//                .setStartDate(start.getTime())
//                .setEndDate(end.getTime())
//                .build()
//
//            val generator = KeyPairGenerator.getInstance(KEY_ALGORITHM_RSA, AndroidKeyStore);
//            generator.initialize(spec)
//            generator.generateKeyPair()
//        }
//
//    }

    fun encrypt(password: String): String {
        val secretKeySpec = SecretKeySpec(password.toByteArray(), "AES")
        val iv = ByteArray(16)
        val charArray = password.toCharArray()
        for (i in 0 until charArray.size){
            iv[i] = charArray[i].toByte()
        }
        val ivParameterSpec = IvParameterSpec(iv)

        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec)

        val encryptedValue = cipher.doFinal(password.toByteArray())
        return Base64.encodeToString(encryptedValue, Base64.DEFAULT)
    }

    fun decrypt(password: String): String {
        val secretKeySpec = SecretKeySpec(password.toByteArray(), "AES")
        val iv = ByteArray(16)
        val charArray = password.toCharArray()
        for (i in 0 until charArray.size){
            iv[i] = charArray[i].toByte()
        }
        val ivParameterSpec = IvParameterSpec(iv)

        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec)

        val decryptedByteValue = cipher.doFinal(Base64.decode(password, Base64.DEFAULT))
        return String(decryptedByteValue)
    }


}