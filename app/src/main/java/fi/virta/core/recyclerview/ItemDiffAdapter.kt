package fi.virta.core.recyclerview

import androidx.recyclerview.widget.DiffUtil
import fi.virta.ui.main.AdapterItem

/**
 * Diff Utils callback for comparing objects in the recycler view while updating items
 * from another source. Used to avoid frequent ui updates for the users when it is not necessary.
 * Uses [AdapterItem] that contains different types of possible recycler view items.
 */
object ItemDiffCallback : DiffUtil.ItemCallback<AdapterItem>()
{
    override fun areItemsTheSame(oldItem: AdapterItem,
                                 newItem: AdapterItem) =
        oldItem.isTheSameItem(newItem)

    override fun areContentsTheSame(oldItem: AdapterItem,
                                    newItem: AdapterItem) =
        oldItem.isTheContentSame(newItem)
}