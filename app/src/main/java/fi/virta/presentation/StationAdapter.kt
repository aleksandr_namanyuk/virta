package fi.virta.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import fi.virta.R
import fi.virta.core.recyclerview.DataBoundListAdapter
import fi.virta.core.recyclerview.ItemDiffCallback
import fi.virta.data.model.Station
import fi.virta.databinding.ItemStationBinding
import fi.virta.ui.main.AdapterItem
import fi.virta.ui.main.AdapterItem.Companion.ITEM_TYPE_STATION
import fi.virta.ui.main.ItemStation
import org.joda.time.DateTimeUtils
import timber.log.Timber

class StationAdapter(
    private val callback: ((Station) -> Unit)?
) : DataBoundListAdapter<AdapterItem, ViewDataBinding>(
    diffCallback = ItemDiffCallback
) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val layoutInflater = LayoutInflater.from(parent.context)

        Timber.v("Create Binding for : $viewType")
        val binding = when(viewType){
            ITEM_TYPE_STATION -> DataBindingUtil.inflate<ItemStationBinding>(
                layoutInflater,
                R.layout.item_station,
                parent,
                false
            )
            else -> DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_station,
                parent,
                false
            )
        }

        binding.root.setOnClickListener {
            when(binding) {
                is ItemStationBinding -> {

                    Timber.v(" -> ItemStationBinding§")

                    binding.station?.let { station ->
                        callback?.invoke(station)
//                        callback?.onOpenPublication(publication)
                    }

                }
            }
        }


        return binding
    }

    override fun bind(binding: ViewDataBinding, item: AdapterItem, position: Int) {
        when(binding) {
            is ItemStationBinding -> {
                item as ItemStation
                Timber.v("bind : ${item.station.id}")
                binding.station = item.station

                item.station.evses?.forEachIndexed { index, evses ->

                    evses.connectors?.let {
                        if(it.isNotEmpty()) {

                            Timber.v(it[0].maxkw.toString())

                            when (index) {
                                0 -> binding.k1 = it[0].maxkw.toString()
                                1 -> binding.k2 = it[0].maxkw.toString()
                                2 -> binding.k3 = it[0].maxkw.toString()
                                3 -> binding.k4 = it[0].maxkw.toString()
                            }
                        }
                    }


                }

            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).itemType
    }

    fun buildStationCollectionFromData(
        data: List<Station>?
    ): List<AdapterItem>? {

        val readyList = arrayListOf<AdapterItem>()

        data?.forEach { station ->

            Timber.v("Station id : ${station.id}")
            Timber.v("city : ${station.city}")
            val itemStation = ItemStation(station)
            readyList.add(itemStation)
        }

        return readyList
    }

}